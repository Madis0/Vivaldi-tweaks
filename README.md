# CSS tweaks for Vivaldi

A list of tweaks I have made for the [Vivaldi browser](https://vivaldi.net). Some tweaks may not be compatible with each other so test, read comments and adjust code as needed. The tweaks are free to use, modify and republish, unless written otherwise per-file.

For more tweaks, check out [official forum](https://forum.vivaldi.net/category/52/modifications) or [this compilation](https://hadden89.vivaldi.net/2018/06/06/vivaldi-mods-css-js/). I also make [tweaks for Firefox](https://gitlab.com/Madis0/Firefox-tweaks).

### How to use the tweaks

1. Open `vivaldi://experiments`
2. Enable "Allow for using CSS modifications"
3. Open Settings > Appearance (`vivaldi://settings/appearance/`)
4. Choose a folder to import CSS files from
5. Create a CSS file inside the folder, add styles to it
6. Restart Vivaldi to see the styles

### How to inspect Vivaldi's UI

Permanent context menu option

1. Go to `vivaldi://flags`
2. Enable **#debug-packed-apps**
3. Restart Vivaldi
4. Right click on any UI element, select **Inspect**

Temporary, direct access

1. Go to `vivaldi://inspect`
2. Select **Apps**
3. On Vivaldi, select **inspect**

### How to adjust toolbar buttons

As of 2.6:
* Address bar leftmost buttons and status bar buttons can be removed with right click > Customize > Remove From Toolbar
* Address bar leftmost buttons and status bar buttons can be swapped with shift+drag
* Address and status bar can be reset by right click > Customize > Reset Toolbar To Default
* Extension buttons can be swapped with a drag and hidden with right click > Hide button
* Extension overflow button behaviour can be toggled from address bar settings
* Profile button can be hidden from address bar settings - `vivaldi://settings/addressbar/`
* Menu button/bar can be toggled in appearance settings - `vivaldi://settings/appearance/`
* Bookmarks bar behaviour can be adjusted in bookmarks settings - `vivaldi://settings/bookmarks/`

### How to use the Chromium UI

Obviously this prevents using all of the tweaks listed here and most of the Vivaldi's unique features, but it might come useful for some cases, like testing similarities with Chrome.

1. Open `vivaldi://about`
2. Copy the full path of `Target`
3. Close Vivaldi completely
4. Open a terminal/command prompt window
5. Paste the path, add `--app` to the end
6. Vivaldi will open in a Chromium-like window
7. If you want this permanently (no guarantees on how it affects Vivaldi), update any shortcuts used to launch it by adding `--app` to the end

### Useful internal pages

#### Vivaldi

* `vivaldi://settings/all` ➡️ search `🐶` - hidden "dogfood" settings
* `vivaldi://experiments` - experimental settings
* `vivaldi://themecolors` - browser UI CSS variables, for mods like in this repo
* [More Vivaldi hidden pages](http://cqoicebordel.free.fr/vivaldi/)
 
#### Chromium

* `chrome://settings/people` - main settings
* `chrome://flags` - experimental settings [(list of useful ones)](https://gitlab.com/Madis0/hidden-settings/blob/master/chrome.md)
* `chrome://system` - system info and internal extensions
* `chrome://vivaldi-urls` - all Chromium hidden pages

